CREATE DATABASE  IF NOT EXISTS `lost-pet` /*!40100 DEFAULT CHARACTER SET latin1 */;
USE `lost-pet`;
-- MySQL dump 10.13  Distrib 8.0.13, for Win64 (x86_64)
--
-- Host: 127.0.0.2    Database: lost-pet
-- ------------------------------------------------------
-- Server version	5.5.5-10.1.29-MariaDB

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
 SET NAMES utf8 ;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `breed`
--

DROP TABLE IF EXISTS `breed`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
 SET character_set_client = utf8mb4 ;
CREATE TABLE `breed` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(45) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=184 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `breed`
--

LOCK TABLES `breed` WRITE;
/*!40000 ALTER TABLE `breed` DISABLE KEYS */;
INSERT INTO `breed` VALUES (1,'Poodle'),(2,'Ciobanesc Belgian'),(3,'Soricar'),(4,'Metis'),(183,'Ciobanesc German');
/*!40000 ALTER TABLE `breed` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `category`
--

DROP TABLE IF EXISTS `category`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
 SET character_set_client = utf8mb4 ;
CREATE TABLE `category` (
  `id` bigint(20) NOT NULL,
  `name` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `category`
--

LOCK TABLES `category` WRITE;
/*!40000 ALTER TABLE `category` DISABLE KEYS */;
/*!40000 ALTER TABLE `category` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `chat`
--

DROP TABLE IF EXISTS `chat`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
 SET character_set_client = utf8mb4 ;
CREATE TABLE `chat` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `sender_id` int(11) NOT NULL,
  `dest_id` int(11) NOT NULL,
  `chat_name` varchar(45) NOT NULL,
  `multiplication` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `index4` (`dest_id`,`sender_id`),
  UNIQUE KEY `unqmult` (`multiplication`),
  KEY `idsnd` (`sender_id`),
  CONSTRAINT `iddst` FOREIGN KEY (`dest_id`) REFERENCES `user` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `idsnd` FOREIGN KEY (`sender_id`) REFERENCES `user` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=228 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `chat`
--

LOCK TABLES `chat` WRITE;
/*!40000 ALTER TABLE `chat` DISABLE KEYS */;
INSERT INTO `chat` VALUES (221,1,184,'tusertudortest',3128),(227,224,1,'ususert',3808);
/*!40000 ALTER TABLE `chat` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `chat_comment`
--

DROP TABLE IF EXISTS `chat_comment`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
 SET character_set_client = utf8mb4 ;
CREATE TABLE `chat_comment` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `id_chat` int(11) NOT NULL,
  `message` mediumtext NOT NULL,
  `date` datetime DEFAULT NULL,
  `sender_username` varchar(45) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `idchat` (`id_chat`),
  CONSTRAINT `idchat` FOREIGN KEY (`id_chat`) REFERENCES `chat` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=230 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `chat_comment`
--

LOCK TABLES `chat_comment` WRITE;
/*!40000 ALTER TABLE `chat_comment` DISABLE KEYS */;
INSERT INTO `chat_comment` VALUES (222,221,'buna tudortest','2019-02-13 00:39:42','t'),(223,221,'buna t','2019-02-13 00:41:31','tudortest'),(228,227,'asdasd','2019-02-13 09:11:52','us'),(229,227,'asdfsdf','2019-02-13 09:12:23','t');
/*!40000 ALTER TABLE `chat_comment` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `comment`
--

DROP TABLE IF EXISTS `comment`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
 SET character_set_client = utf8mb4 ;
CREATE TABLE `comment` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `listing` int(11) NOT NULL,
  `user` int(11) NOT NULL,
  `comment` mediumtext NOT NULL,
  `date` datetime NOT NULL,
  PRIMARY KEY (`id`),
  KEY `id_anunt` (`listing`),
  KEY `id_user` (`user`),
  CONSTRAINT `comment_ibfk_1` FOREIGN KEY (`listing`) REFERENCES `listing` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `comment_ibfk_2` FOREIGN KEY (`user`) REFERENCES `user` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=227 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `comment`
--

LOCK TABLES `comment` WRITE;
/*!40000 ALTER TABLE `comment` DISABLE KEYS */;
INSERT INTO `comment` VALUES (2,74,2,'comentriul lui doi','2019-02-09 13:09:50'),(88,74,1,'comentariu','2018-06-12 19:31:00'),(89,74,1,'comentariu','2018-06-12 19:31:00'),(90,74,1,'asdasd','2019-02-09 12:56:16'),(91,74,1,'asdas','2019-02-09 12:57:56'),(93,74,1,'aasdasd','2019-02-09 13:08:37'),(94,74,1,'asdasd','2019-02-09 13:09:18'),(95,74,1,'asdasd','2019-02-09 13:09:50'),(96,77,1,'comentariu la anunt','2019-02-09 13:46:39'),(97,74,1,'comentariu','2018-06-12 19:31:00'),(98,74,1,'comentariu','2018-06-12 19:31:00'),(99,74,1,'comentariu','2018-06-12 19:31:00'),(100,74,1,'comentariu','2018-06-12 19:31:00'),(101,75,1,'am gasit cainele','2019-02-09 15:51:04'),(102,78,1,'Catre utilizatorul W , am gasit cainele','2019-02-09 15:55:32'),(105,77,2,'Sunt w si ti-am gasit catelul','2019-02-09 16:08:09'),(111,107,1,'asdads','2019-02-09 23:26:23'),(112,107,1,'asdadsasdasd','2019-02-09 23:26:55'),(113,75,2,'am gasti acest caine','2019-02-10 20:31:02'),(166,165,3,'poate ti-am gasit cainele','2019-02-11 19:31:27'),(186,74,184,'ti-am gasit cainele','2019-02-13 00:00:46'),(226,75,224,'sdfsdf','2019-02-13 09:11:18');
/*!40000 ALTER TABLE `comment` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `hibernate_sequence`
--

DROP TABLE IF EXISTS `hibernate_sequence`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
 SET character_set_client = utf8mb4 ;
CREATE TABLE `hibernate_sequence` (
  `next_val` bigint(20) DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `hibernate_sequence`
--

LOCK TABLES `hibernate_sequence` WRITE;
/*!40000 ALTER TABLE `hibernate_sequence` DISABLE KEYS */;
INSERT INTO `hibernate_sequence` VALUES (233),(233),(233);
/*!40000 ALTER TABLE `hibernate_sequence` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `listing`
--

DROP TABLE IF EXISTS `listing`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
 SET character_set_client = utf8mb4 ;
CREATE TABLE `listing` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `writer_id` int(11) NOT NULL,
  `title` varchar(45) DEFAULT NULL,
  `abstract_field` varchar(255) DEFAULT NULL,
  `body` varchar(255) DEFAULT NULL,
  `breed_id` int(11) NOT NULL,
  `lat` varchar(45) NOT NULL,
  `lng` varchar(45) NOT NULL,
  `img_url` varchar(45) DEFAULT 'dog-face.png',
  `lat_end` varchar(45) NOT NULL,
  `lng_end` varchar(45) NOT NULL,
  `pierdut` tinyint(4) NOT NULL,
  `adress` varchar(45) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `id_idx1` (`breed_id`),
  KEY `writer_id` (`writer_id`),
  CONSTRAINT `writer_id` FOREIGN KEY (`writer_id`) REFERENCES `user` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=233 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `listing`
--

LOCK TABLES `listing` WRITE;
/*!40000 ALTER TABLE `listing` DISABLE KEYS */;
INSERT INTO `listing` VALUES (74,1,'titlu3_modificat','asdasd','dasdadsasdasdasdasda',1,'47.0496052197085','21.917586119708517','1549747921692.jpg','47.0523031802915','21.92028408029148',0,'Oradea Zoo'),(75,1,'titlu3','asdasd','dasdadsasdasdasdasda',1,'46.7054762','23.52254400000004','11549577803195.jpg','23.52254400000004','23.710480899999993',0,'asdasda'),(77,1,'anunt_edited','abstract_anunt','am pierdut caine in oradea',1,'47.0070067','21.842536999999993','11549631555282.jpg','21.842536999999993','22.032351500000004',0,'asdadadadadadada'),(78,2,'am pierdut caine la plaja','am pierdut caine la plaja','am pierdut caine la plaja',3,'25.709042','-80.31975990000001','1549645073941.jpg','25.855773','-80.13915700000001',0,'Miami'),(106,2,'Am pierdut un caine','am pierdut cainele colorat','am pierdut cainele colorat in bucuresti',2,'44.3342445','25.963700099999983','1549743380421.jpg','44.541407','26.225574999999935',1,'Bucharest'),(107,2,'am pierdut caine la munte','am pierdut caine la munte','mi-am pierdut cainele in nepal',3,'27.9968067','86.84183985000004','1549743621607.jpg','28.0045127','86.85640045000002',1,'Everest Base Camp'),(156,3,'Mi-am pierdut cainele','mi-am pierdut cainele in timisoara cred','mi-am pierdut cainele in timisoara si ma numesc qt?',3,'45.6838101','21.135377999999946','1549905207547.jpg','45.798319','21.297801599999957',0,'Timi?oara'),(165,164,'Am gasit un caine in oradea','Am gasit un caine colorat','am gasti un caine colorat in oradea',4,'47.0070067','21.842536999999993','1549906197498.jpg','47.14218289999999','22.032351500000004',0,'Oradea'),(185,184,'Am pierdut caine care arata ca o pisica','Am pierdut caine care arata ca o pisica','am pierdut caine care arata ca o pisica',1,'46.7054762','23.52254400000004','1550008576552.jpg','46.8612951','23.710480899999993',1,'Cluj-Napoca'),(225,224,'asdasd','asdasd','aasdads',2,'46.7054762','23.52254400000004','1550041854229.jpg','46.8612951','23.710480899999993',0,'Cluj-Napoca'),(230,1,'asd','asd','asdasd',1,'51.5274781197085','-0.27095788029146206','1553444233319.jpg','51.5301760802915','-0.2682599197084983',1,'Asda Park Royal Superstore'),(231,1,'asd','asd','asdad',1,'53.4601580197085','-2.247700880291518','1553448196101.jpg','53.4628559802915','-2.2450029197084405',1,'Asda Hulme Superstore'),(232,1,'asdad','asdasd','asdasd',1,'26.1461570697085','-105.6096508802915','1553448270356.jpg','26.1488550302915','-105.60695291970853',1,'SDASDASD');
/*!40000 ALTER TABLE `listing` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `listing_categories`
--

DROP TABLE IF EXISTS `listing_categories`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
 SET character_set_client = utf8mb4 ;
CREATE TABLE `listing_categories` (
  `id` int(11) NOT NULL,
  `categories_id` varchar(45) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `listing_categories`
--

LOCK TABLES `listing_categories` WRITE;
/*!40000 ALTER TABLE `listing_categories` DISABLE KEYS */;
/*!40000 ALTER TABLE `listing_categories` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `listing_related_listings`
--

DROP TABLE IF EXISTS `listing_related_listings`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
 SET character_set_client = utf8mb4 ;
CREATE TABLE `listing_related_listings` (
  `id` int(11) NOT NULL,
  `listing_id` int(11) NOT NULL,
  `title` varchar(45) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `listing_related_listings`
--

LOCK TABLES `listing_related_listings` WRITE;
/*!40000 ALTER TABLE `listing_related_listings` DISABLE KEYS */;
/*!40000 ALTER TABLE `listing_related_listings` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `user`
--

DROP TABLE IF EXISTS `user`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
 SET character_set_client = utf8mb4 ;
CREATE TABLE `user` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `password` varchar(255) DEFAULT NULL,
  `role` varchar(45) DEFAULT 'USER',
  `username` varchar(255) NOT NULL,
  `address` varchar(200) DEFAULT NULL,
  `created` timestamp(6) NOT NULL DEFAULT CURRENT_TIMESTAMP(6) ON UPDATE CURRENT_TIMESTAMP(6),
  `email` varchar(200) NOT NULL,
  `telephone` varchar(50) DEFAULT NULL,
  `img_url` varchar(45) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `UC_Username` (`username`)
) ENGINE=InnoDB AUTO_INCREMENT=225 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `user`
--

LOCK TABLES `user` WRITE;
/*!40000 ALTER TABLE `user` DISABLE KEYS */;
INSERT INTO `user` VALUES (1,'t','ADMIN','t','baritiu 12asdasd','2019-02-11 18:30:03.741380','cabautudor@gmail.com','0772111113','poza_1.jpg'),(2,'w','USER','w','dorobanti','2019-02-09 18:30:39.461247','user3@gmail.com','0773333333','poza_2.jpg'),(3,'qT','USER','qT','baritiu 24 noua adresa','2019-02-11 15:15:24.000000','dumb@dummy.com','0771111112','poza_3.jpg'),(33,'asdad','USER','sdads','asdasd','2019-02-06 21:08:29.815642','cabautudor@gmail.com','0774974977',''),(34,'a','USER','a','adresaa','2019-02-06 21:09:22.962644','a@email','0324234234',''),(164,'t','USER','tudorcgb','adresa lui tudor','2019-02-11 17:25:22.000000','cabautudor@gmail.com','0774974977','poza_3.jpg'),(184,'t','USER','tudorNonAdmin','adresatest','2019-02-12 21:45:50.000000','cabautudor@gmail.com','774974977','poza_184.jpg'),(224,'t','USER','ust','asdasdas','2019-02-13 07:08:32.000000','cabautudor@gmail.com','774974977','poza_224.jpg');
/*!40000 ALTER TABLE `user` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2019-04-25 17:59:59
