﻿


(function () {
    'use strict';


    angular
        .module('app', ['ngRoute', 'ngCookies','ngMaterial','xeditable'])
        .config(config)
        .run(run);

    config.$inject = ['$routeProvider', '$locationProvider'];
    function config($routeProvider, $locationProvider) {
        $routeProvider
            .when('/', {
                controller: 'HomeController',
                templateUrl: 'home/home.view.html',
                controllerAs: 'vm'
            })

            .when('/login', {
                controller: 'LoginController',
                templateUrl: 'login/login.view.html',
                controllerAs: 'vm'
            })

            .when('/register', {
                controller: 'RegisterController',
                templateUrl: 'register/register.view.html',
                controllerAs: 'vm'
            })
            .when('/gasit', {
                controller: 'PierdutController',
                templateUrl: 'pierdut/pierdut.view.html',
                controllerAs: 'vm'
            })
            .when('/pierdut', {
                controller: 'PierdutController',
                templateUrl: 'pierdut/pierdut.view.html',
                controllerAs: 'vm'
            })
            .when('/profil', {
                controller: 'ProfilController',
                templateUrl: 'profil/profil.view.html',
                controllerAs: 'vm'
            })
            .when('/profil_details', {
                controller: 'ProfilController',
                templateUrl: 'profil/profil_details.view.html',
                controllerAs: 'vm'
            })
            .when('/anunt', {
                controller: 'AnuntController',
                templateUrl: 'anunt/anunt.view.html',
                controllerAs: 'vm'

            })
            .when('/places', {
                controller: 'AnuntController',
                templateUrl: 'places_test/places.view.html',
                controllerAs: 'vm'
            })
            .when('/anunturile_mele', {
                controller: 'AnunturileMeleController',
                templateUrl: 'anunturile_mele/anunturile_mele.view.html',
                controllerAs: 'vm'

            })
            .when('/edit_anunt', {
            controller: 'AnuntController',
            templateUrl: 'anunt/anunt.view.html',
            controllerAs: 'vm'
            })
            .when('/chat', {
                controller: 'ChatController',
                templateUrl: 'chat/chat.view.html',
                controllerAs: 'vm'
            })
            .when('/detalii_anunt/:idAnunt', {
                controller: 'AnuntDetailsController',
                templateUrl: 'anunt_details/anunt_details.view.html',
                controllerAs: 'vm'
            })
            .when('/detalii_user/:idUser', {
                controller: 'ProfilController',
                templateUrl: 'profil/profil_details.view.html',
                controllerAs: 'vm'
            })
            .when('/admin', {
                controller: 'AdminController',
                templateUrl: 'admin/admin.view.html',
                controllerAs: 'vm'
            })
            .when('/admin/breeds', {
                controller: 'BreedController',
                templateUrl: 'admin/breed/breed.view.html',
                controllerAs: 'vm'
            })
            .when('/admin/listings', {
                controller: 'ListingsController',
                templateUrl: 'admin/listings/listings.view.html',
                controllerAs: 'vm'
            })

            .otherwise({ redirectTo: '/login' });
    }

    run.$inject = ['$rootScope', '$location', '$cookies', '$http'];
    function run($rootScope, $location, $cookies, $http, $scope) {

        // keep user logged in after page refresh
        $rootScope.globals = $cookies.getObject('globals') || {};
        if ($rootScope.globals.currentUser) {
            $http.defaults.headers.common['Authorization'] = 'Basic ' + $rootScope.globals.currentUser.authdata;
        }

        $rootScope.loggedIn = $rootScope.globals.currentUser;


        $rootScope.$on('$locationChangeStart', function (event, next, current) {

            if(( $location.url().indexOf('/admin') > -1) && !$rootScope.globals.isAdmin){
                $location.path('/login');
            }

            //todo remove '/adauga anunt'
            // redirect to login page if not logged in and trying to access a restricted page
            var restrictedPage = true;
            var allowedPages = ['/login', '/register', '/gasit', '/pierdut','/places','/detalii_anunt'];
            var url = $location.url();
            allowedPages.forEach(function(element){
                if(url.indexOf(element) > -1){
                    console.log(element);
                    restrictedPage = false;
                }
            });
            //var restrictedPage = $.inArray($location.path(), ['/login', '/register', '/gasit', '/pierdut','/places','/detalii_anunt']) === -1;
            var loggedIn = $rootScope.globals.currentUser;
            $rootScope.loggedIn = $rootScope.globals.currentUser;
            $rootScope.isAdmin = $rootScope.globals.isAdmin;

            if (restrictedPage && !loggedIn) {
                $location.path('/login');
            }

        });
    }
    // // Smooth scrolling using jQuery easing
    // $('a.js-scroll-trigger[href*="#"]:not([href="#"])').click(function() {
    //     if (location.pathname.replace(/^\//, '') == this.pathname.replace(/^\//, '') && location.hostname == this.hostname) {
    //         var target = $(this.hash);
    //         target = target.length ? target : $('[name=' + this.hash.slice(1) + ']');
    //         if (target.length) {
    //             $('html, body').animate({
    //                 scrollTop: (target.offset().top - 54)
    //             }, 1000, "easeInOutExpo");
    //             return false;
    //         }
    //     }
    // });

    // Closes responsive menu when a scroll trigger link is clicked
    $('.js-scroll-trigger').click(function() {
        $('.navbar-collapse').collapse('hide');
    });

    // Activate scrollspy to add active class to navbar items on scroll
    $('body').scrollspy({
        target: '#mainNav',
        offset: 56
    });

    // Collapse Navbar
    var navbarCollapse = function() {
        if ($("#mainNav").offset().top > 100) {
            $("#mainNav").addClass("navbar-shrink");
        } else {
            $("#mainNav").removeClass("navbar-shrink");
        }
    };
    // Collapse now if page is not at top
    navbarCollapse();
    // Collapse the navbar when page is scrolled
    $(window).scroll(navbarCollapse);


})();