(function () {
    'use strict';

    angular
        .module('app')
        .factory('ChatService', ChatService);

    ChatService.$inject = ['$http'];
    function ChatService($http) {
        var service = {};


        service.GetByChatsForUser = GetByChatsForUser;
        service.Create = Create;
        service.Delete = Delete;

        return service;
        function GetByChatsForUser(userId) {
            return $http.get('http://tudorcabau.go.ro:8089/chat/' + userId).then(handleSuccess, handleError('Error getting chats for user'));
        }

        function Create(newChat) {
            return $http.post('http://tudorcabau.go.ro:8089/chat/save', newChat, {
                headers: {'Content-Type': 'application/json'},
                transformRequest: angular.identity
            })
                .then(handleSuccess, handleError('Error creating chat'));
        }

        function Delete(id) {
            return $http.delete('http://tudorcabau.go.ro:8089/comments/' + id).then(handleSuccess, handleError('Error deleting user'));
        }

        // private functions

        function handleSuccess(res) {
            res.success = true;
            return res;
        }

        function handleError(error) {
            return function () {
                return { success: false, message: error };
            };
        }
    }

})();
