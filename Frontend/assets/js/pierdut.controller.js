



  var app = angular.module('plunkr', [])

  //angular.module('demo', [])
  app.controller('Hello', function($scope, $http, $location) {
      $http.get('http://tudorcabau.go.ro:8089/listing/list').
          then(function(response) {
              $scope.listings = response.data;
              //$scope.imageSrc = "http://localhost:63342/LostPet/backend_main/templates/" + response.data.imgUrl;
              $scope.users = ['Fabio', 'Leonardo', 'Thomas', 'Gabriele', 'Fabrizio', 'John', 'Luis', 'Kate', 'Max','Pula'];
          });

        $scope.goLogin = function(listingId) {
        // window.location.replace('http://127.0.0.1:3000/listingDetail.html');

         window.location = '/listingDetail.html?id=' + listingId;
     };
  });
  //angular.module('MyApp')
    app.controller('AppCtrl', function($scope) {
      $scope.users = ['Fabio', 'Leonardo', 'Thomas', 'Gabriele', 'Fabrizio', 'John', 'Luis', 'Kate', 'Max','Pula'];
    });
    app.controller('UploadController', function($scope,$http, fileReader) {
      $scope.uploadFile = function() {
            var fd = new FormData();
            $scope.advert = {"writer":"1"};

            fd.append("file", $scope.myFileFile);

            $http.post("http://localhost:8089/images/", fd, {

                headers: {'Content-Type': undefined },
                transformRequest: angular.identity
            });


            $http.post("http://localhost:8089/listing/save", $scope.advert, {

                headers: {'Content-Type': undefined },
                transformRequest: angular.identity
            });
        };
      $http.get('http://tudorcabau.go.ro:8089/breeds/list').
          then(function(response) {
              $scope.rase = response.data;
              //$scope.imageSrc = "http://localhost:63342/LostPet/backend_main/templates/" + response.data.imgUrl;

          });
      $scope.imageSrc = "../../assets/img/meeting.jpg";

      $scope.$on("fileProgress", function(e, progress) {
        $scope.progress = progress.loaded / progress.total;
      });
    });

    app.directive("ngFileSelect", function(fileReader, $timeout) {
      return {
        scope: {
          ngModel: '='

        },
        link: function($scope, el) {
          function getFile(file) {
            $scope.myFileFile = file;
            fileReader.readAsDataUrl(file, $scope)
              .then(function(result) {
                $timeout(function() {
                  $scope.ngModel = result;
                });
              });
          }

          el.bind("change", function(e) {
            var file = (e.srcElement || e.target).files[0];
            getFile(file);
            //$scope.myFileFile = file;
          });
        }
      };
    });

  app.factory("fileReader", function($q, $log) {
    var onLoad = function(reader, deferred, scope) {
      return function() {
        scope.$apply(function() {
          deferred.resolve(reader.result);
        });
      };
    };

    var onError = function(reader, deferred, scope) {
      return function() {
        scope.$apply(function() {
          deferred.reject(reader.result);
        });
      };
    };

    var onProgress = function(reader, scope) {
      return function(event) {
        scope.$broadcast("fileProgress", {
          total: event.total,
          loaded: event.loaded
        });
      };
    };

    var getReader = function(deferred, scope) {
      var reader = new FileReader();
      reader.onload = onLoad(reader, deferred, scope);
      reader.onerror = onError(reader, deferred, scope);
      reader.onprogress = onProgress(reader, scope);
      return reader;
    };

    var readAsDataURL = function(file, scope) {
      var deferred = $q.defer();

      var reader = getReader(deferred, scope);
      reader.readAsDataURL(file);

      return deferred.promise;
    };

    return {
      readAsDataUrl: readAsDataURL
    };
  });
//   app.config(function($stateProvider) {
//     $stateProvider
//         .state('first', {
//             url: '/first',
//             templateUrl: 'first.html'
//         })
//         .state('second', {
//             url: '/second/:id',
//             templateUrl: 'second.html'
//         })
// });
