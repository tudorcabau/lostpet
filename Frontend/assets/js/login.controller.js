(function () {
    'use strict';

    angular
        .module('LostPet')
        .controller('LoginController', LoginController);

    LoginController.$inject = ['$location', 'AuthenticationService', 'FlashService'];
    function LoginController($location, AuthenticationService, FlashService) {
        //$scope.stringTest = "lala";
        var vm = this;

        vm.login = login;

        (function initController() {
            // reset login status
            AuthenticationService.ClearCredentials();
        })();

        function login() {
            vm.dataLoading = true;
            AuthenticationService.Login(vm.username, vm.password, function (response) {
                if (response.success) {
                    AuthenticationService.SetCredentials(vm.username, vm.password);
                    window.location = "index.html"
                } else {
                    FlashService.Error(response.message);
                    vm.dataLoading = false;
                }
            });
        };
    }

})();
// var app = angular.module('plunkr', [])
//
// //angular.module('demo', [])
// app.controller('Hello', function($scope, $http, $location) {
//     $http.get('http://tudorcabau.go.ro:8089/listing/list').
//         then(function(response) {
//             $scope.stringTest = "lala";
//             //$scope.imageSrc = "http://localhost:63342/LostPet/backend_main/templates/" + response.data.imgUrl;
//             $scope.users = ['Fabio', 'Leonardo', 'Thomas', 'Gabriele', 'Fabrizio', 'John', 'Luis', 'Kate', 'Max','Pula'];
//         });
//
//       //$scope.goLogin = function(listingId) {
//       // window.location.replace('http://127.0.0.1:3000/listingDetail.html');
//
//        //window.location = '/listingDetail.html#?id=' + listingId;
//    };
// });
