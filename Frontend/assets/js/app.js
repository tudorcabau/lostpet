
(function($) {
  "use strict"; // Start of use strict
  angular
         .module('LostPet', ['ngRoute', 'ngCookies'])
         .config(config)
         .run(run);

     config.$inject = ['$routeProvider', '$locationProvider'];
     function config($routeProvider, $locationProvider) {
         $routeProvider
             // .when('/', {
             //     controller: 'HomeController',
             //     templateUrl: 'index.html',
             //     controllerAs: 'vm'
             // })

             // .when('/login.html', {
             //     controller: 'LoginController',
             //     templateUrl: 'login.html',
             //     controllerAs: 'vm'
             // })
             //
             // .when('/register', {
             //     controller: 'RegisterController',
             //     templateUrl: 'registration.html',
             //     controllerAs: 'vm'
             // })

             //.otherwise({ redirectTo: '/login' });
     }

     run.$inject = ['$rootScope', '$location', '$cookieStore', '$http'];
     function run($rootScope, $location, $cookieStore, $http) {
         // keep user logged in after page refresh
         $rootScope.globals = $cookieStore.get('globals') || {};
         if ($rootScope.globals.currentUser) {
             $http.defaults.headers.common['Authorization'] = 'Basic ' + $rootScope.globals.currentUser.authdata; // jshint ignore:line
         }

         $rootScope.$on('$locationChangeStart', function (event, next, current) {
             // redirect to login page if not logged in and trying to access a restricted page
             var restrictedPage = $.inArray($location.path(), ['/login', '/register']) === -1;
             var loggedIn = $rootScope.globals.currentUser;
             // if (restrictedPage && !loggedIn) {
             //     $location.path('/login');
             // }
         });
     }
     // // Smooth scrolling using jQuery easing
     // $('a.js-scroll-trigger[href*="#"]:not([href="#"])').click(function() {
     //   if (location.pathname.replace(/^\//, '') == this.pathname.replace(/^\//, '') && location.hostname == this.hostname) {
     //     var target = $(this.hash);
     //     target = target.length ? target : $('[name=' + this.hash.slice(1) + ']');
     //     if (target.length) {
     //       $('html, body').animate({
     //         scrollTop: (target.offset().top - 54)
     //       }, 1000, "easeInOutExpo");
     //       return false;
     //     }
     //   }
     // });
     //
     // // Closes responsive menu when a scroll trigger link is clicked
     // $('.js-scroll-trigger').click(function() {
     //   $('.navbar-collapse').collapse('hide');
     // });
     //
     // // Activate scrollspy to add active class to navbar items on scroll
     // $('body').scrollspy({
     //   target: '#mainNav',
     //   offset: 56
     // });
     //
     // // Collapse Navbar
     // var navbarCollapse = function() {
     //   if ($("#mainNav").offset().top > 100) {
     //     $("#mainNav").addClass("navbar-shrink");
     //   } else {
     //     $("#mainNav").removeClass("navbar-shrink");
     //   }
     // };
     // // Collapse now if page is not at top
     // navbarCollapse();
     // // Collapse the navbar when page is scrolled
     // $(window).scroll(navbarCollapse);
     //
     // // Hide navbar when modals trigger
     // $('.portfolio-modal').on('show.bs.modal', function(e) {
     //   $(".navbar").addClass("d-none");
     // })
     // $('.portfolio-modal').on('hidden.bs.modal', function(e) {
     //   $(".navbar").removeClass("d-none");
     // })

})(jQuery); // End of use strict
