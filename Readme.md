# Welcome to LostDog!

Hi! This web application wants to help dog owners find their lost dog. 


## Post 

Lost dog allows you to post a new ad, for either a dog you lost or dog you found. 

![Post](ReadMe/images/post.jpg)

## Login

![Login](ReadMe/images/login.jpg)

In order to post a new add you need to login, and fill basic contact information.

##  Chat

You can chat with other user, via a real time chat application inside the website. 

![Chat](ReadMe/images/chat.jpg)

## Search ads

You can find ads using filters and word search. The current filters are: breed, location (using Google Maps ) and dog size.

![Listing](ReadMe/images/listing.jpg)

##  Edit

You can edit your own ads.

![Profile](ReadMe/images/profile.jpg)

##  Contact

A person that sees your ad can comment, or contact you via email or phone number.


## Edit information

You can edit your own account information, like username, phone, password etc.
